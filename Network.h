#pragma once
#include <QtCore>
#include <QtNetwork>

class Network : public QObject {
	Q_OBJECT
public:
    explicit Network(QObject *parent = 0);

	public slots:
	bool connectToHost(QString host, int port);
	bool writeData(QString data);
    void dataRecievedFromSocket();

signals:
    void dataReceived(QString);

private slots:
	void error(QAbstractSocket::SocketError error);

private:
    QTcpSocket *sendSocket_;
	QString host_;
	QByteArray buffer_;
	qint32 size_;
};
