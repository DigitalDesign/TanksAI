#include <QCoreApplication>
#include <string>
#include "Network.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/memorybuffer.h"
#include <random>
#include "templates.h"
#include "GameMap.h"
using namespace rapidjson;

GameMap map;
float move,turn,shot;
QTimer gameTimer;
Network* network;


void play(){
    //dastresi be x y , r  2 tank
     qDebug()<<map.tank1.x<<" "<<map.tank1.y<<" "<<map.tank1.r;
      qDebug()<<map.tank2.x<<" "<<map.tank2.y<<" "<<map.tank2.r;

    /*
    //be ezaye har derakht
    for(int i=0;i<map.treesPos.size();i++){
        std::pair<float,float> tree = map.treesPos[i];
        //dastresi be x,y har derakht
        qDebug()<<tree.first<<" "<<tree.second;
    }
    */

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1,1);
    move=dis(gen);
    turn=dis(gen);
    shot=std::abs(dis(gen));
}


void GameLoop(){
    play();
    Document doc;
    doc.Parse(CommandTemplate.c_str());

    doc["FloatArray"].PushBack(move, doc.GetAllocator());
    doc["FloatArray"].PushBack(turn, doc.GetAllocator());
    doc["FloatArray"].PushBack(shot, doc.GetAllocator());

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    network->writeData(QString::fromStdString(std::string(buffer.GetString())));
    qDebug()<<"Sending ";
    gameTimer.stop();
    gameTimer.start();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    network=new Network();

    gameTimer.setInterval(160);
    gameTimer.stop();


    if( network->connectToHost("127.0.0.1",3000))
        qDebug()<<"Connect successfully ! ";

    network->writeData(GameRequestString);

    QObject::connect(network,
                     &Network::dataReceived, &map,&GameMap::handleNetWorkInput);
    QObject::connect(&map,&GameMap::GameStart,[&](){gameTimer.start();});

    QObject::connect(&gameTimer,&QTimer::timeout,GameLoop);

    return a.exec();
}
