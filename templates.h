#ifndef TEMPLATES
#define TEMPLATES

const std::string GAME_REQUEST = "Game_Request";
const std::string GAME_ACTION = "Game_Action";
const std::string GAME_MAP = "Game_Map";

const std::string CommandTemplate="{"
                                  "\"version\": \"1.0\","
                                  "\"CommandType\":\"Game_Map\", "
                                  "\"FloatArray\": [] ,"
                                  "\"IntArray\": [],"
                                  "\"StringArray\": []"
                                  "}";

const QString GameRequestString=QString::fromStdString("{"
                                                       "\"version\": \"1.0\","
                                                       "\"CommandType\":\""+GAME_REQUEST+"\", "
                                                                                         "\"FloatArray\": [] ,"
                                                                                         "\"IntArray\": [],"
                                                                                         "\"StringArray\": []"
                                                                                         "}");
#endif // TEMPLATES

